const puppeteer = require('puppeteer');
const request = require('request')

//should be more dynamic.. but here i've hardcoded an exisiting URL if the URL is not provided in the parameter when running the script. 
let bookingUrl = 'https://secure.lucky2go.com/flights/options/?partner_id=LUCKY2GO&v6=8023502d-a380-4cf2-8c4d-64e1fe821f88%21RoundTrip%21economy%211008.9%21EUR%2111%211%210%210%210%21PAR%2CPOL%21POL%2CPAR%21search%21%21%21ODMgsEULyq992z6Fba0OCTDyEDHdzANck_y681lBdqEy9I1odTFL4dnkUAl8EWCvOtLTFKwp4YEPo31Zy7gWmmXaKuQEuAv-JzFOBeT8wOQ_KwJkZ5mCD6XiX2yxQ0cB4LMP4A9nLH8d0r289x9aTrIr8pB3P99LNH9L_lO658oW9WRg-VSfUktxELRXFg_PM4NQqW_2MgRh-vaiPC-ga4Tt3o6Zifs3D33jyDOe-gHIp5Tb90249MuXwJ-YpSjF-tIirqFPDsGp5naT7gncGqcS7euSS4HqbtcvctUnt3Vz6uhZOGpKxq9A7e6E09LKmZ7uvo-Xo23UyRKq-Ei8IyCpzJVdatdKY0NX-tnkv_hyKjshiky2KMg4CRF_XTvcWk-uUaKgxFIQXWurcdx5lqLhLPLtkF4AweUzOmsPleRcr5Gvm-QuiNqbqh-zo78xzmSd4hBhfQ-cBPAOjTQmmlDcmlb_b2vfxAId2rzAoaAfwy2plKJSxR0X5y7_hCl3F.dpDYQln2HRRrP813ywaOD7wQaID0kjzwbtHAjqdxfe6r5-rptc0ywxZFKQRrW6cIJWY2XJVqg9B9X-TUM5lKSL4VPHzy8ou-MxQ9aMavor1EQzjYDOCn2YTp9U4GfYoZNX09qt1deYEvmRbUXR-mM9gntu1Zkv-2vl3fnMQVshWl9JZVyZWk815Vdd15chOkklfWpS0TK1QB_s-1uE7B9RAtWqnqkEHUotzH8XMkIEHmtkHMZ6CIM2z9nnWoC9kfo_4nTrT_fiCmERfLWFJfZRjXnzMFfxNGqvi9b5SkAavfQkWWqV0vyWHbcOftqJ_lvue9mVNQ0SW5KLCNirSX0T5FURsNF7jk5GR_ptLgsuOzEfAQemv_qS0ySf6ArDOQuILxlpUqHAYMn2fsLXpZL0sasLqImLM_ZQwYzmzbpwvl-Kw9Kr3AgycV0ydXYzhDgwHVlFsKj1NPwab1_zsuVOJHH5kbRRHe18-DstTfmJ7QkgLhqbxfVUdM8caezHmo6&lf=1009&atr=0A';

if (process.argv.length>2){
    bookingUrl = process.argv[2];
}

(async () => {
    const browser = await puppeteer.launch({ headless: true }); //can set headless to false if you want to see what the script is doing in the browser
    const page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 926 });
    await page.goto(bookingUrl, { waitUntil: 'networkidle2' });
    await page.waitForSelector(".open-leg-details-container");

    let itinerary = {};

    let flightData = await page.evaluate(() => {
        
        let flightDetails = {};      
        let totalCost = {}
        
        totalCost.price = document.querySelector('div[class="price-wrapper"] > .total-price-amount > span[class="amount"]').innerText
        totalCost.currency = document.querySelector('div[class="price-wrapper"] > .total-price-amount > span[class="currency"]').innerText

        //get details of flights by clicking each of the details button
        buttons = document.querySelectorAll("a[class='leg-details open-leg-details']")
        buttons.forEach((button, index)=>{
            
            let flightTrips = []
            button.click()  
                try{
                //find the components which popped up after clicking the details button
                let flightDetailsComponent = document.querySelectorAll('.component-leg-details')[index]

                //wait for 8 seconds to ensure all the elements are loaded into the DOM
                setTimeout(function(){return true;},800);
                
                //this gets each of the segment details of the trip from the pop up window.
                let trips = flightDetailsComponent.querySelectorAll('div.segment');
                
                //get each separate flight detail for each of the flights (i.e. in the case of different layovers)
                trips.forEach((tripSegment)=>{
                    let trip = {};
                    let departure = {}
                    let arrival = {}
                    departure.time = tripSegment.querySelector('span[class="qa-segment-departure-time"]').innerText;
                    departure.date = tripSegment.querySelectorAll("p.airport > strong > span")[1].getAttribute("data-qa-segment-departure-date")
                    departure.location = tripSegment.querySelector('span[class="airport-name qa-segment-departure-airport"]').innerText;
                    arrival.time = tripSegment.querySelector('span[class="qa-segment-arrival-time"]').innerText;
                    arrival.date = tripSegment.querySelectorAll("p.airport > strong > span")[3].getAttribute("data-qa-segment-arrival-date")
                    arrival.location = tripSegment.querySelector('span[class="airport-name qa-segment-arrival-airport"]').innerText;
                    trip.duration = tripSegment.querySelector('div[class="segment-info"] > p > strong.qa-segment-flight-time').innerText;
                    trip.airline = tripSegment.querySelector('div[class="segment-info"] > p > strong.qa-segment-airline-name').innerText;
                    trip.flightNum = tripSegment.querySelector('div[class="segment-info"] > p > strong.qa-segment-flight-number').innerText;
                    trip.class = tripSegment.querySelector('div[class="segment-info"] > p > strong.qa-segment-service-class').innerText
                    trip.depart = departure
                    trip.arrival = arrival

                    flightTrips.push(trip);
                });

                flightDetails[index] = flightTrips;
            }
            catch (error){
                console.error("Error loading data after button click:", error)
            }
        });
        return [flightDetails, totalCost]
    });

    itinerary.tripDetails = flightData[0]
    itinerary.totalCost = flightData[1]
    let itineraryJSON = JSON.stringify(itinerary);
    console.dir(itineraryJSON);

    //post data to API
    postJSON(itineraryJSON)

    await browser.close();
})();

function postJSON(itineraryJSON){
    // //POST request: sending json formatted data to api
    request.post('​api.capturedata.ie', { json: itineraryJSON }, (error, res, body) => {    
        if (error) {
            console.error("Error in POST request.", error)
            return
        }
        console.log(`statusCode: ${res.statusCode}`)
        // console.log(body)
    a})
}