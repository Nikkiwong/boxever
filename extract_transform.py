import csv
import requests
import sys
from flask import json

def create_json_obj(titles,flight_values):
    zip_obj = zip(titles,flight_values)
    return dict(zip_obj)

def post_to_endpoint(data):
    header = {"Content-Type": "application/json"}
    response= requests.post('​api.capturedata.ie',data=json.dumps(data), headers=header)
    if response.status_code != 200:
        raise Exception('POST return status {}: {}'.format(response.status_code, response.reason))

def main():
    with open('output_data.json', 'w') as f:
        flight_details = []
        output_json_data = []
        with open('input_data.csv') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            for row in readCSV:
                flight_details.append(row)
            for i in range(1,len(flight_details)):
                flight_detail_obj = create_json_obj(flight_details[0], flight_details[i])
                post_to_endpoint(flight_detail_obj)
                output_json_data.append(flight_detail_obj)
        json.dump(output_json_data, f)

main()